#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_LEN 20

enum options {
	PRINT_LINE = 1, ADD_TO_LINE, REMOVE_FROM_LINE, ADD_VIP, SEARCH_IN_LINE, REVERSE_LINE, EXIT
};

typedef struct personNode
{
	char name[STR_LEN];
	int age;
	struct personNode* next;
}personNode;

personNode* createPerson(char name[], int age);
void myFgets(char str[STR_LEN]);
int listLength(personNode** head);
void printList(personNode* head);
void insertAtEnd(personNode** head, personNode* newNode);
void addVIP(personNode** head, personNode* newNode);
void deleteNode(personNode** head, char* name);
void searchPrson(personNode** head, char* name);
void revers(personNode** head);
int addToLine(personNode** head, personNode* newNode);
void freeListRecursive(personNode** head);
int getChoice();

int main(void)
{
	personNode* head = NULL;
	personNode* curr = NULL;
	char name[STR_LEN] = { 0 };
	int age = 0;
	int listLen = 0;
	int i = 0;
	int choice = 0;
	while (choice != EXIT)
	{
		choice = getChoice();
		switch (choice)
		{
		case PRINT_LINE:
			printf("%d people in line:\n", listLength(&head));
			printList(head);
			break;
		case ADD_TO_LINE:
			printf("Enter name: ");
			myFgets(name);
			printf("Enter age: ");
			scanf("%d", &age);
			getchar();
			curr = createPerson(name, age);
			if (!addToLine(&head, curr))
			{
				insertAtEnd(&head, curr);
			}
			break;
		case REMOVE_FROM_LINE:
			printf("Enter name to remove:\n");
			myFgets(name);
			deleteNode(&head, name);
			break;
		case ADD_VIP:
			printf("Enter name: ");
			myFgets(name);
			printf("Enter age: ");
			scanf("%d", &age);
			getchar();
			curr = createPerson(name, age);
			addVIP(&head, curr);
			break;
		case SEARCH_IN_LINE:
			printf("Enter name to search:\n");
			myFgets(name);
			searchPrson(&head, name);
			break;
		case REVERSE_LINE:
			revers(&head);
			break;
		}
	}
	printf("Goodbye!");

	freeListRecursive(&head);

	getchar();
	return 0;
}


/*
Function will create a person node
input: person name, age
output: the person node
*/
personNode* createPerson(char name[], int age)
{
	personNode* newSong = (personNode*)malloc(sizeof(personNode));

	strncpy(newSong->name, name, STR_LEN);
	newSong->age = age;
	newSong->next = NULL;

	return newSong;
}


/*
Function will print the line
input: the line (the head)
output: none
*/
void printList(personNode* head)
{
	personNode* curr = head;
	while (curr) // not the end
	{
		printf("Name: %s, Age: %d \n", curr->name, curr->age);
		curr = curr->next;
	}
}


/**
Function will add a person node to the end of list
input: the head and the node to add
output: none
*/
void insertAtEnd(personNode** head, personNode* newNode)
{
	personNode* curr = *head;
	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		while (curr->next) // while the nnot the last node
		{
			curr = curr->next;
		}

		curr->next = newNode;
		newNode->next = NULL;
	}
}

/*
Function will delete a persone from the line
input: the head and the name of person to delete
output: none
*/
void deleteNode(personNode** head, char* name)
{
	personNode* curr = *head;
	personNode* temp = NULL;
	int removed = 0;
	// if the list is not empty 
	if (*head)
	{
		//should the first node be deleted?
		if (0 == strcmp((*head)->name, name))
		{
			*head = (*head)->next;
			free(curr);
			removed = 1;
		}
		else
		{
			while (curr->next)
			{
				// waiting to be on the node BEFORE the one to delete
				if ((0 == strcmp(curr->next->name, name)))
				{
					//delete
					temp = curr->next;
					curr->next = temp->next;
					free(temp);
					removed = 1;
				}
				else
				{
					curr = curr->next;
				}
			}
		}
	}
	if (removed)
	{
		printf("%s removed from line\n", name);
	}
	else
	{
		printf("%s not in line\n", name);
	}
}



/*
Function will free all memory of the line
input: head
output: none
*/
void freeListRecursive(personNode** head)
{
	if (*head != NULL) // if list not empty
	{
		if ((*head)->next != NULL) // end condition
		{
			freeListRecursive(&((*head)->next));
		}

		free(*head);
	}
}

/*
function will gert the line length
input: the head
output: the length
*/
int listLength(personNode** head)
{
	if (*head != NULL) // if list not empty
	{
		if ((*head)->next != NULL) // end condition
		{
			return listLength(&((*head)->next)) + 1;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}

/*
prints the menu and get the choice
input: none
output: the choice
*/
int getChoice()
{
	int choice = 0;
	printf("\nLine Management Software!\n");
	printf("Please enter your choice from the following options:\n");
	printf("1 - Print line\n");
	printf("2 - Add person to line\n");
	printf("3 - Remove person from line\n");
	printf("4 - VIP guest\n");
	printf("5 - Search in line\n");
	printf("6 - Reverse line\n");
	printf("7 - Exit\n");
	scanf("%d", &choice);
	getchar();
	return choice;
}
/*
function add petson to line (israely way)
input: head of line and the person node to add
output: if been added or not
*/
int addToLine(personNode** head, personNode* newNode)
{
	personNode* curr = *head;
	char friends[STR_LEN][STR_LEN] = { 0 };
	int added = 0;
	int i = 0;
	printf("Enter names of 3 friends:\n");

	for (i = 0; i < 3; i++)
	{
		printf("Friend %d :", i + 1);
		myFgets(friends[i]);

	}
	// if the list is not empty 
	if (*head)
	{
		while (curr->next)
		{
			if (!strcmp(curr->name, friends[0]) || !strcmp(curr->name, friends[1]) || !strcmp(curr->name, friends[2]))
			{
				newNode->next = curr->next;
				curr->next = newNode;
				added = 1;
			}
			curr = curr->next;
		}
	}
	return added;
}
/*
function adds a VIP person to the front of the line
input: the head and the person node to add
output: none
*/
void addVIP(personNode** head, personNode* newNode)
{
	personNode* temp = (*head);
	(*head) = newNode;
	newNode->next = temp;
}
/*
function search for a persone in line
input: the head and the name to search
output:
*/
void searchPrson(personNode** head, char* name)
{
	personNode* curr = *head;
	int found = 0;
	// if the list is not empty 
	if (*head)
	{
		while (curr)
		{
			if ((!strcmp(curr->name, name))) // waiting to be on the node
			{
				found = 1;
			}
			curr = curr->next;
		}
	}
	if (found)
	{
		printf("%s found in line\n", name);
	}
	else
	{
		printf("%s not in line\n", name);
	}
}
/*
function revers the line
inout: head
output: noen
*/
void revers(personNode** head)
{
	personNode* prev = NULL;
	personNode* curr = *head;
	personNode* nextNode = *head;
	while (nextNode)
	{
		nextNode = nextNode->next;
		curr->next = prev;
		prev = curr;
		curr = nextNode;
	}
	(*head) = prev;
}
/*
function gets a string from the user into a string
input: the buffer
output: none
*/
void myFgets(char str[STR_LEN])
{
	fgets(str, STR_LEN, stdin);
	if (strlen(str) < STR_LEN)
	{
		str[strlen(str) - 1] = '\0';
	}
}