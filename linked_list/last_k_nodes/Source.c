#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

typedef struct IntNode
{
	int val;
	struct IntNode* next;
} IntNode;

IntNode* lastKnodes(IntNode* head, int k);
IntNode* createNode(int num);
void getVAlues(IntNode** head);
void insertAtEnd(IntNode** head, IntNode* newNode);
void printList(IntNode* list);
void freeList(IntNode** list);
int main(void)
{
	IntNode* list = NULL;
	IntNode* curr = NULL;
	int k = 0;
	getVAlues(&list);
	printf("Choose a number k, and the k last values will be printed: ");
	scanf("%d", &k);
	getchar();
	printf("The list from that node to the end:\n");
	curr = lastKnodes(list, k);
	printList(curr);

	freeList(&list);
	getchar();
	return 0;
}
/*
gets a pointer to the K'th node from the end
input: head of list and k
output: the poiter to the K'th node from the end
runing time: 1(n) (first loop k - 1, second loop n - (k - 1) )
*/
IntNode* lastKnodes(IntNode* head, int k)
{
	IntNode* front = NULL;
	IntNode* back = NULL;
	int i = 0;
	front = back = head;
	//moving the front k-1 times 
	for (i = 0; i < k - 1; i++) {
		front = front->next;
	}
	//movint the two pointers together till the front one wiil
	//reach end and the back one will be k times befor the end 
	while (front->next)
	{
		front = front->next;
		back = back->next;
	}

	return back;
}
/*
prints a linked list
input: head
output: none
*/
void printList(IntNode* list)
{
	if (list)
	{
		printf("%d  ", list->val);
		printList(list->next);
	}
	else
	{
		printf("\n");
	}
}
/*
creating the list
input: pointer to the head of the list
output: none
*/
void getVAlues(IntNode** head)
{
	int numOfNodes = 0;
	int i = 0;
	int num = 0;
	printf("How many nodes in list? ");
	scanf("%d", &numOfNodes);
	getchar();
	for (i = 0; i < numOfNodes; i++)
	{
		printf("Enter number: ");
		scanf("%d", &num);
		getchar();
		insertAtEnd(head, createNode(num));

	}

}
/*
free all the list
input: pointer to the head of list
output: none
*/
void freeList(IntNode** list)
{
	if (*list != NULL) // if list not empty
	{
		if ((*list)->next != NULL) // end condition
		{
			freeList(&((*list)->next));
		}
		free(*list);
	}
}

/*
Function will create a node
input: value
output: the person node
*/
IntNode* createNode(int num)
{
	IntNode* newNode = (IntNode*)malloc(sizeof(IntNode));

	newNode->val = num;
	newNode->next = NULL;

	return newNode;
}

/**
Function will add a node to the end of list
input: pointer to the head and the node to add
output: none
*/
void insertAtEnd(IntNode** head, IntNode* newNode)
{
	IntNode* curr = *head;
	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		while (curr->next) // while the nnot the last node
		{
			curr = curr->next;
		}

		curr->next = newNode;
		newNode->next = NULL;
	}
}